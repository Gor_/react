import React, { useState, useEffect } from "react";
import Header from "./components/Header/Header";
import { Route, Routes } from "react-router-dom";
import MainPage from "./Pages/MainPage/MainPage";
import BasketPage from "./Pages/BasketPage/BasketPage";
import FavoritePage from "./pages/FavoritePage/FavoritePage";

function App() {
  const [data, setData] = useState([]);
  const [cart, setCart] = useState([]);
  const [fav, setFav] = useState([]);
  // const [article, setArticle] = useState(0);
  // const [isModalOpen, setIsModalOpen] = useState(false);

  useEffect(() => {
    const favor = localStorage.getItem("fav");
    const cart = localStorage.getItem("cart");
    if (favor) {
      setFav(JSON.parse(favor));
    }
    if (cart) {
      setCart(JSON.parse(cart));
    }
    fetch("./data.json")
      .then((r) => r.json())
      .then((jewelry) => setData(jewelry));
  }, [data]);

  // function toggleModal() {
  //   setIsModalOpen((prevState) => !prevState);
  // }

  // function articleOnload(article) {
  //   setArticle(article);
  //   toggleModal();
  // }

  // function addToFav(article) {
  //   if (fav.includes(article)) {
  //     setFav(fav.splice(fav.indexOf(article), 1));
  //   } else {
  //     setFav(fav.push(article));
  //   }
  //   setFav(fav);
  //   localStorage.setItem("fav", JSON.stringify(fav));
  // }

  // function addToCart() {
  //   setCart((prevCart) => {
  //     const newCart = [...prevCart, article];
  //     localStorage.setItem("cart", JSON.stringify(newCart));
  //     return newCart;
  //   });
  //   toggleModal();
  // }

  // function removeBasketCard() {
  //   const articleIndex = cart.indexOf(article);
  //   if (articleIndex !== -1) {
  //     const refresh = [...cart];
  //     refresh.splice(articleIndex, 1);
  //     setCart(refresh);
  //     localStorage.setItem("cart", JSON.stringify(refresh));
  //   }
  //   toggleModal();
  // }

  return (
    <>
      <Header favAmt={fav.length} cartAmt={cart.length} />
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="/fav" element={<FavoritePage />} />
        <Route path="/basket" element={<BasketPage />} />
      </Routes>
    </>
  );
}

export default App;
