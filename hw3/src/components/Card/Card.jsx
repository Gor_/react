import React from "react";
import style from "./card.module.scss";

function Card(props) {
  const { name, price, url, article, color, onClick, actions, isFav } = props;
  return (
    <div className={style.card__wrapper} id={article}>
      <img src={url} alt="#" width="350px" height="250px" />

      <img
        className={style.fav__icon}
        onClick={onClick}
        src={isFav ? "../../img/yellow-favor.png" : "../../img/favor-icon.png"}
        alt="#"
        width="35px"
        height="25px"
      />

      <h2 className={style.name}>{name}</h2>
      <p className={style.price}>{price}</p>
      <div className={style.color} style={{ background: color }}></div>
      {actions}
    </div>
  );
}

export default Card;
