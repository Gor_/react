import React from "react";
import style from "./CardList.module.scss";
import Card from "../Card/Card";
import Button from "../Button/Button";

function CardList(props) {
  const { cards, fav, cart, addToCart, addToFav } = props;

  return (
    <ul className={style.cards__list}>
      {cards.map(({ name, price, url, article, color }) => (
        <li key={article} className={style.card__item}>
          <Card
            name={name}
            price={price}
            url={url}
            article={article}
            color={color}
            isFav={fav.includes(article)}
            isCart={cart.includes(article)}
            onClick={() => addToFav(article)}
            actions={
              <>
                <Button
                  className={style.add__button}
                  text="Add to Basket"
                  onClick={() => addToCart(article)}
                />
              </>
            }
          />
        </li>
      ))}
    </ul>
  );
}

export default CardList;
