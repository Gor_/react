// import React from "react";
// import { Route, Routes } from "react-router-dom";
// import MainPage from "./Pages/MainPage/MainPage";
// import FavoritePage from "./Pages/FavoritePage/FavoritePage";
// import BasketPage from "./Pages/BasketPage/BasketPage";

// function Router(props) {
//   const {
//     cart,
//     fav,
//     data,
//     setCart,
//     setArticle,
//     addToFav,
//     addToCart,
//     articleOnload,
//     isModalOpen,
//     toggleModal,
//     removeBasketCard,
//   } = props;
//   return (
//     <>
//       <Routes>
//         <Route
//           path="/"
//           element={
//             <MainPage
//               cart={cart}
//               fav={fav}
//               data={data}
//               addToFav={addToFav}
//               addToCart={addToCart}
//               articleOnload={articleOnload}
//               isModalOpen={isModalOpen}
//               toggleModal={toggleModal}
//             />
//           }
//         />
//         <Route
//           path="/fav"
//           element={<FavoritePage fav={fav} cards={data} addToFav={addToFav} />}
//         />

//         <Route
//           path="/basket"
//           element={
//             <BasketPage
//               data={data}
//               cart={cart}
//               setCart={setCart}
//               setArticle={setArticle}
//               articleOnload={articleOnload}
//               toggleModal={toggleModal}
//               isModalOpen={isModalOpen}
//               removeBasketCard={removeBasketCard}
//             />
//           }
//         />
//       </Routes>
//     </>
//   );
// }

// export default Router;
