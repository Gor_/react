import React from "react";
import style from "./Basket.module.scss";
import Card from "../../components/Card/Card";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";

function BasketPage(props) {
  const { data, cart, articleOnload, isModalOpen, toggleModal } = props;

  function removeBasketCard(article) {
    const articleIndex = cart.indexOf(article);
    if (articleIndex !== -1) {
      const refresh = [...cart];
      refresh.splice(articleIndex, 1);
      setCart(refresh);
      localStorage.setItem("cart", JSON.stringify(refresh));
    }
    toggleModal();
  }

  return (
    <>
      <h1 className={style.title}>Cart</h1>
      <div className={style.container}>
        <ul className={style.basket__list}>
          {data &&
            data.map(({ name, price, url, article, color }) => {
              if (cart.includes(article)) {
                return (
                  <li key={article} className={style.basket__item}>
                    <Card
                      name={name}
                      price={price}
                      url={url}
                      color={color}
                      actions={
                        <button
                          className={style.cross__btn}
                          onClick={() => articleOnload(article)}
                        >
                          <span className={style.cross__span}>X</span>
                        </button>
                      }
                    />
                  </li>
                );
              }
              return null;
            })}
        </ul>
      </div>

      {isModalOpen && (
        <>
          <Modal
            className={style.modal__wrapper}
            header={style.modal__title}
            title="Do you want to delete this item?"
            warning="Are you sure you want to delete this item from cart?"
            backgroundColor={{ backgroundColor: "#F0EAD6" }}
            onClick={toggleModal}
            actions={
              <div className={style.button__container}>
                <Button
                  text="Yes"
                  className={style.modal__button}
                  onClick={() => {
                    removeBasketCard(articleOnload);
                  }}
                />
                <Button
                  text="Cancel"
                  className={style.modal__button}
                  onClick={toggleModal}
                />
              </div>
            }
          />
        </>
      )}
    </>
  );
}

export default BasketPage;
