import React, { useState, useEffect } from "react";
import Modal from "../../components/Modal/Modal";
import CardList from "../../components/CardList/CardList";
import Button from "../../components/Button/Button";
import style from "../../App.module.scss";

function MainPage(props) {
  // const {
  //   cart,
  //   fav,
  //   data,
  //   addToFav,
  //   addToCart,
  //   articleOnload,
  //   toggleModal,
  //   isModalOpen,
  // } = props;
  const [data, setData] = useState([]);
  const [cart, setCart] = useState([]);
  const [fav, setFav] = useState([]);
  const [article, setArticle] = useState(0);
  const [isModalOpen, setIsModalOpen] = useState(false);

  useEffect(() => {
    const favor = localStorage.getItem("fav");
    const cart = localStorage.getItem("cart");
    if (favor) {
      setFav(JSON.parse(favor));
    }
    if (cart) {
      setCart(JSON.parse(cart));
    }
    fetch("./data.json")
      .then((r) => r.json())
      .then((jewelry) => setData(jewelry));
  }, [data]);

  function toggleModal() {
    setIsModalOpen((prevState) => !prevState);
  }

  function articleOnload(article) {
    setArticle(article);
    toggleModal();
  }

  function addToFav(article) {
    if (fav.includes(article)) {
      setFav(fav.splice(fav.indexOf(article), 1));
    } else {
      setFav(fav.push(article));
    }
    setFav(fav);
    localStorage.setItem("fav", JSON.stringify(fav));
  }

  function addToCart() {
    setCart((prevCart) => {
      const newCart = [...prevCart, article];
      localStorage.setItem("cart", JSON.stringify(newCart));
      return newCart;
    });
    toggleModal();
  }

  // function removeBasketCard() {
  //   const articleIndex = cart.indexOf(article);
  //   if (articleIndex !== -1) {
  //     const refresh = [...cart];
  //     refresh.splice(articleIndex, 1);
  //     setCart(refresh);
  //     localStorage.setItem("cart", JSON.stringify(refresh));
  //   }
  //   toggleModal();
  // }
  return (
    <>
      <h1 className={style.main__title}>Jewelry</h1>
      <div className={style.card__wrapper}>
        <CardList
          cart={cart}
          fav={fav}
          cards={data}
          addToFav={addToFav}
          addToCart={articleOnload}
        />
      </div>

      {isModalOpen && (
        <>
          <Modal
            className={style.modal__wrapper}
            header={style.modal__title}
            title="Do you want to add this item?"
            warning="Are you sure you want to add it?"
            onClick={toggleModal}
            backgroundColor={{ backgroundColor: "#F0EAD6" }}
            actions={
              <div className={style.button__container}>
                <Button
                  text="Ok"
                  className={style.modal__button}
                  onClick={addToCart}
                />
                <Button
                  text="Cancel"
                  className={style.modal__button}
                  onClick={toggleModal}
                />
              </div>
            }
          />
        </>
      )}
    </>
  );
}

export default MainPage;
