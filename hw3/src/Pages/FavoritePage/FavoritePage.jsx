import React from "react";
import style from "./favor.module.scss";
import Card from "../../components/Card/Card";

function FavoritePage(props) {
  const { cards, fav, addToFav } = props;

  return (
    <div>
      <h1 className={style.fav__title}>Favorites</h1>
      <div className={style.fav__container}>
        <ul className={style.fav__list}>
          {cards &&
            cards.map(({ name, price, url, article, color }) => {
              if (fav.includes(article)) {
                return (
                  <li key={article} className={style.fav__item}>
                    <Card
                      name={name}
                      price={price}
                      url={url}
                      color={color}
                      onClick={() => addToFav(article)}
                    />
                  </li>
                );
              }
            })}
        </ul>
      </div>
    </div>
  );
}

export default FavoritePage;
