import { getProducts } from "./actions/actions";

function fetchProducts() {
  return function (dispatch) {
    fetch("./data.json")
      .then((r) => r.json())
      .then((products) => dispatch(getProducts(products)))
      .catch((error) => console.error(error));
  };
}

export default fetchProducts;
