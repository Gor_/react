import { combineReducers } from "redux";
import productsReducer from "./productsReducer";
import modalReducer from "./modalReducer";

export const rootReducer = combineReducers({
  products: productsReducer,
  modal: modalReducer,
});
