import { OPEN_MODAL, CLOSE_MODAL } from "../actions/modal";

const initialState = {
  isModalOpen: false,
};

function modalReducer(state = initialState, action) {
  switch (action.type) {
    case OPEN_MODAL:
      return { ...state, isModalOpen: true };

    case CLOSE_MODAL:
      return { ...state, isModalOpen: false };

    default:
      return state;
  }
}
export default modalReducer;
