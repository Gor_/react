import {
  GET_PRODUCTS,
  ADD_PRODUCT_TO_FAVORITES,
  REMOVE_PRODUCT_FROM_FAVORITES,
  ADD_PRODUCT_TO_CART,
  REMOVE_PRODUCT_FROM_CART,
} from "../actions/actions";

const initialFavs = localStorage.getItem("fav");
const initialCart = localStorage.getItem("cart");

const initialState = {
  products: [],
  fav: initialFavs ? JSON.parse(initialFavs) : [],
  cart: initialCart ? JSON.parse(initialCart) : [],
};

function productsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_PRODUCTS:
      return { ...state, products: action.payload };

    case ADD_PRODUCT_TO_FAVORITES:
      const { fav } = state;
      const article = action.payload;
      if (fav.includes(article)) {
        fav.splice(fav.indexOf(article), 1);
      } else {
        fav.push(article);
      }
      localStorage.setItem("fav", JSON.stringify(fav));
      return { ...state, fav: fav };

    case REMOVE_PRODUCT_FROM_FAVORITES:
      const favs = [...state.fav];
      const articleToRemove = action.payload;
      const newFav = favs.filter((article) => article !== articleToRemove);
      localStorage.setItem("fav", JSON.stringify(newFav));
      return { ...state, fav: newFav };

    case ADD_PRODUCT_TO_CART:
      const { cart } = state;
      const newProduct = action.payload;
      const newCart = [...cart, newProduct];
      localStorage.setItem("cart", JSON.stringify(newCart));
      return { ...state, cart: newCart };

    case REMOVE_PRODUCT_FROM_CART:
      const prevCart = [...state.cart];
      const removeArticle = action.payload;
      const index = prevCart.findIndex(
        (item) => item.article === removeArticle
      );
      if (index !== -1) {
        prevCart.splice(index, 1);
      }
      localStorage.setItem("cart", JSON.stringify(prevCart));
      return { ...state, cart: prevCart };

    default:
      return state;
  }
}

export default productsReducer;
