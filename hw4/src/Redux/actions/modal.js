export const OPEN_MODAL = "open/modal";
export const CLOSE_MODAL = "close/modal";

export const openModal = () => ({
  type: OPEN_MODAL,
});

export const closeModal = () => ({
  type: CLOSE_MODAL,
});
