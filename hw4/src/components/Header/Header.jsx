import React from "react";
import style from "./header.module.scss";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

function Header() {
  const { fav, cart } = useSelector((state) => state.products);

  return (
    <header className={style.page__header}>
      <nav className={style.nav}>
        <Link className={style.nav__link} to={"/"}>
          Main
        </Link>
        <Link className={style.nav__link} to={"/fav"}>
          Favorites
        </Link>
        <Link className={style.nav__link} to={"/cart"}>
          Cart
        </Link>
      </nav>

      <div className={style.header__icons}>
        <img
          className={style.cart__icon}
          src="../../../img/shopping-bag.png"
          alt="Cart"
          width="40px"
          height="40px"
        />
        <p className={style.cart__amt}>{cart.length}</p>
        <img
          className={style.fav__icon}
          src="../../../img/yellow-favor.png"
          alt="Favorite"
          width="35px"
          height="35px"
        />
        <p className={style.fav__amt}>{fav.length}</p>
      </div>
    </header>
  );
}

export default Header;
