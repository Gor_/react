import React from "react";
import style from "./Basket.module.scss";
import Card from "../../components/Card/Card";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";
import { removeCartCard } from "../../Redux/actions/actions";
import { useDispatch, useSelector } from "react-redux";
import { closeModal, openModal } from "../../Redux/actions/modal";

function BasketPage() {
  const { isModalOpen } = useSelector((state) => state.modal);
  const { products, cart } = useSelector((state) => state.products);
  const dispatch = useDispatch();

  return (
    <>
      <h1 className={style.title}>Cart</h1>
      <div className={style.container}>
        <ul className={style.basket__list}>
          {products.map(({ name, price, url, article, color }) => {
            if (cart.includes(article)) {
              return (
                <li key={article} className={style.basket__item}>
                  <Card
                    name={name}
                    price={price}
                    url={url}
                    color={color}
                    actions={
                      <button
                        className={style.cross__btn}
                        onClick={() => dispatch(openModal())}
                      >
                        <span className={style.cross__span}>X</span>
                      </button>
                    }
                  />
                </li>
              );
            }
          })}
        </ul>
      </div>

      {isModalOpen && (
        <>
          <Modal
            className={style.modal__wrapper}
            header={style.modal__title}
            title="Do you want to delete this item?"
            warning="Are you sure you want to delete it?"
            backgroundColor={{ backgroundColor: "#F0EAD6" }}
            onClick={() => dispatch(closeModal())}
            actions={
              <div className={style.button__container}>
                <Button
                  text="Yes"
                  className={style.modal__button}
                  onClick={() => {
                    dispatch(removeCartCard());
                    dispatch(closeModal());
                  }}
                />
                <Button
                  text="Cancel"
                  className={style.modal__button}
                  onClick={() => dispatch(closeModal())}
                />
              </div>
            }
          />
        </>
      )}
    </>
  );
}

export default BasketPage;
