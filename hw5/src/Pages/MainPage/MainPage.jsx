import React from "react";
import CardList from "../../components/CardList/CardList";
import style from "../../App.module.scss";

function MainPage() {
  return (
    <>
      <h1 className={style.main__title}>Jewelry</h1>
      <div className={style.card__wrapper}>
        <CardList />
      </div>
    </>
  );
}

export default MainPage;
