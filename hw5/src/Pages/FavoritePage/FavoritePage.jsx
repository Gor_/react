import React from "react";
import style from "./favor.module.scss";
import Card from "../../components/Card/Card";
import { removeFromFavorites } from "../../Redux/actions/actions";
import { useDispatch, useSelector } from "react-redux";

function FavoritePage() {
  const { fav, products } = useSelector((state) => state.products);
  const dispatch = useDispatch();

  return (
    <div>
      <h1 className={style.fav__title}>Favorites</h1>
      <div className={style.fav__container}>
        <ul className={style.fav__list}>
          {products.map(({ name, price, url, article, color }) => {
            if (fav.includes(article)) {
              return (
                <li key={article} className={style.fav__item}>
                  <Card
                    name={name}
                    price={price}
                    url={url}
                    color={color}
                    isFav={"../../img/yellow-favor.png"}
                    onClick={() => dispatch(removeFromFavorites(article))}
                  />
                </li>
              );
            }
          })}
        </ul>
      </div>
    </div>
  );
}

export default FavoritePage;
