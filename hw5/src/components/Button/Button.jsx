import React from "react";

function Button(props) {
  const { text, className, onClick, backgroundColor } = props;
  return (
    <>
      <button
        style={{ backgroundColor }}
        className={className}
        onClick={onClick}
      >
        {text}
      </button>
    </>
  );
}

export default Button;
