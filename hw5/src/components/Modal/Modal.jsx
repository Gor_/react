import React from "react";
import style from "./Modal.module.scss";

function Modal(props) {
  const {
    className,
    header,
    text,
    actions,
    title,
    warning,
    onClick,
    backgroundColor,
  } = props;

  return (
    <>
      <div style={backgroundColor} className={className}>
        <header className={style.modal__header}>
          <h2 className={header}>{title}</h2>
        </header>

        <div className={style.text__container}>
          <p>{text}</p>
          <p>{warning}</p>
        </div>

        {actions}
      </div>
      <div className={style.modal__bg} onClick={onClick}></div>
    </>
  );
}

export default Modal;
