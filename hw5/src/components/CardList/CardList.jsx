import React, { useState } from "react";
import style from "./CardList.module.scss";
import Card from "../Card/Card";
import Modal from "../../components/Modal/Modal";
import Button from "../Button/Button";
import { addToFav, addToCart } from "../../Redux/actions/actions";
import { useDispatch, useSelector } from "react-redux";
import { openModal, closeModal } from "../../Redux/actions/modal";

function CardList() {
  const { isModalOpen } = useSelector((state) => state.modal);
  const { fav, products } = useSelector((state) => state.products);
  const dispatch = useDispatch();
  const [selectedArticle, setSelectedArticle] = useState(null);

  const articleOnload = (article) => {
    setSelectedArticle(article);
    dispatch(openModal());
  };

  const handleModalOk = () => {
    if (selectedArticle) {
      dispatch(addToCart(selectedArticle));
      setSelectedArticle(null);
    }
    dispatch(closeModal());
  };

  return (
    <>
      <ul className={style.cards__list}>
        {products.map(({ name, price, url, article, color }) => (
          <li key={article} className={style.card__item}>
            <Card
              name={name}
              price={price}
              url={url}
              article={article}
              color={color}
              isFav={fav.includes(article)}
              onClick={() => dispatch(addToFav(article))}
              actions={
                <>
                  <Button
                    className={style.add__button}
                    text="Add to Cart"
                    onClick={() => articleOnload(article)}
                  />
                </>
              }
            />
          </li>
        ))}
      </ul>

      {isModalOpen && (
        <Modal
          className={style.modal__wrapper}
          header={style.modal__title}
          title="Do you want to add this item to cart?"
          warning="Are you sure you want to add it?"
          onClick={() => dispatch(closeModal())}
          backgroundColor={{ backgroundColor: "#F0EAD6" }}
          actions={
            <div className={style.button__container}>
              <Button
                text="Ok"
                className={style.modal__button}
                onClick={handleModalOk}
              />
              <Button
                text="Cancel"
                className={style.modal__button}
                onClick={() => dispatch(closeModal())}
              />
            </div>
          }
        />
      )}
    </>
  );
}

export default CardList;
