import React, { useEffect } from "react";
import Header from "./components/Header/Header";
import MainPage from "./Pages/MainPage/MainPage";
import FavoritePage from "./Pages/FavoritePage/FavoritePage";
import BasketPage from "./Pages/BasketPage/BasketPage";
import { Route, Routes } from "react-router-dom";
import fetchProducts from "./Redux/thunk.js";
import { useDispatch } from "react-redux";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProducts());
  }, []);

  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="/fav" element={<FavoritePage />} />
        <Route path="/cart" element={<BasketPage />} />
      </Routes>
    </>
  );
}

export default App;
