export const GET_PRODUCTS = "get/products";

export const ADD_PRODUCT_TO_FAVORITES = "addProduct/favorites";
export const REMOVE_PRODUCT_FROM_FAVORITES = "remove/favorites";

export const ADD_PRODUCT_TO_CART = "addProduct/cart";
export const REMOVE_PRODUCT_FROM_CART = "removeProduct/cart";

export const getProducts = (article) => ({
  type: GET_PRODUCTS,
  payload: article,
});

export const addToFav = (article) => ({
  type: ADD_PRODUCT_TO_FAVORITES,
  payload: article,
});
export const removeFromFavorites = (article) => ({
  type: REMOVE_PRODUCT_FROM_FAVORITES,
  payload: article,
});

export const addToCart = (article) => ({
  type: ADD_PRODUCT_TO_CART,
  payload: article,
});
export const removeCartCard = (article) => ({
  type: REMOVE_PRODUCT_FROM_CART,
  payload: article,
});
